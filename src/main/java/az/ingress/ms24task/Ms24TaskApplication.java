package az.ingress.ms24task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms24TaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms24TaskApplication.class, args);
	}

}
