package az.ingress.ms24task.service;

import az.ingress.ms24task.domain.ProductEntity;
import az.ingress.ms24task.dto.ProductDto;
import az.ingress.ms24task.dto.ProductRequestDto;
import az.ingress.ms24task.exception.NotFoundException;
import az.ingress.ms24task.repository.ProductRepository;
import az.ingress.ms24task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Override
    public ProductDto create(ProductRequestDto productRequestDto) {

        ProductEntity product = ProductEntity.builder()
                .firstName(productRequestDto.getFirstName())
                .price(productRequestDto.getPrice())
                .stockCount(productRequestDto.getStockCount())
                .build();

        final ProductEntity productEntitySaved = productRepository.save(product);

        return ProductDto.builder()
                .id(productEntitySaved.getId())
                .firstName(productEntitySaved.getFirstName())
                .price(productEntitySaved.getPrice())
                .stockCount(productEntitySaved.getStockCount()).build();
    }

    @Override

    public void delete(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public ProductDto getProductById(Long id) {

        final ProductEntity productEntity = productRepository.findById(id)
                .orElseThrow(NotFoundException::new);

        return ProductDto.builder()
                .id(productEntity.getId())
                .firstName(productEntity.getFirstName())
                .price(productEntity.getPrice())
                .stockCount(productEntity.getStockCount()).build();
    }

    @Override
    public List<ProductDto> getAllUserList() {
        List<ProductEntity> products = productRepository.findAll();
        List<ProductDto> productDtos = new ArrayList<>();

        for (ProductEntity product : products) {
            ProductDto productDto = ProductDto.builder()
                    .id(product.getId())
                    .firstName(product.getFirstName())
                    .price(product.getPrice())
                    .stockCount(product.getStockCount())
                    .build();
            productDtos.add(productDto);
        }

        return productDtos;
    }

    @Override
    public ProductDto updatePriceAndName(Long id, ProductRequestDto productRequestDto) {
        ProductEntity product =
                ProductEntity.builder()
                        .firstName(productRequestDto.getFirstName())
                        .price(productRequestDto.getPrice())
                        .stockCount(productRequestDto.getStockCount())
                        .build();

        final ProductEntity productEntitySaved = productRepository.save(product);

        return ProductDto.builder()
                .id(productEntitySaved.getId())
                .firstName(productEntitySaved.getFirstName())
                .price(productEntitySaved.getPrice())
                .stockCount(productEntitySaved.getStockCount()).build();
    }

    @Override
    public List<ProductDto> buyProductByUser(Long userId, List<Long> productId) {
        return null;
    }
}
