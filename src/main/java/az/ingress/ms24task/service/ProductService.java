package az.ingress.ms24task.service;

import az.ingress.ms24task.dto.ProductDto;
import az.ingress.ms24task.dto.ProductRequestDto;


import java.awt.print.Pageable;
import java.util.List;

public interface ProductService {

        ProductDto create(ProductRequestDto productRequestDto);

        void delete(Long id);


        ProductDto getProductById(Long id);

    List<ProductDto> getAllUserList();

    ProductDto updatePriceAndName(Long id, ProductRequestDto productRequestDto);

    List<ProductDto> buyProductByUser(Long userId, List<Long> productId);
}

