package az.ingress.ms24task.service;
import az.ingress.ms24task.domain.UserEntity;
import az.ingress.ms24task.dto.UserDto;
import az.ingress.ms24task.dto.UserRequestDto;
import az.ingress.ms24task.exception.NotFoundException;
import az.ingress.ms24task.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public UserDto create(UserRequestDto userRequestDto) {
        UserEntity user = UserEntity.builder()
                .userName(userRequestDto.getUserName())
                .firstName(userRequestDto.getFirstName())
                .lastName(userRequestDto.getLastName())
                .age(userRequestDto.getAge())
                .balance(userRequestDto.getBalance())
                .isActive(userRequestDto.getIsActive())
                .build();

        final UserEntity userEntitySaved = userRepository.save(user);

        return UserDto.builder()
                .id(userEntitySaved.getId())
                .userName(userEntitySaved.getUserName())
                .firstName(userEntitySaved.getFirstName())
                .lastName(userEntitySaved.getLastName())
                .age(userEntitySaved.getAge())
                .balance(userEntitySaved.getBalance())
                .isActive(userEntitySaved.getIsActive()).build();
    }

    @Override
    public UserDto getUserById(Long id) {

        final UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(NotFoundException::new);

        return UserDto.builder()
                .id(userEntity.getId())
                .userName(userEntity.getUserName())
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .age(userEntity.getAge())
                .balance(userEntity.getBalance())
                .isActive(userEntity.getIsActive()).build();
    }

    @Override
    public UserDto update(Long id, UserRequestDto userRequestDto) {

        UserEntity user =
                UserEntity.builder()
                        .id(id)
                        .userName(userRequestDto.getUserName())
                        .firstName(userRequestDto.getFirstName())
                        .lastName(userRequestDto.getLastName())
                        .age(userRequestDto.getAge())
                        .balance(userRequestDto.getBalance())
                        .isActive(userRequestDto.getIsActive()).build();

        final UserEntity userEntitySaved = userRepository.save(user);

        return UserDto.builder()
                .id(userEntitySaved.getId())
                .userName(userEntitySaved.getUserName())
                .userName(userEntitySaved.getUserName())
                .lastName(userEntitySaved.getLastName())
                .age(userEntitySaved.getAge())
                .balance(userEntitySaved.getBalance())
                .isActive(userEntitySaved.getIsActive()).build();
    }

    @Override
    public UserDto setIsActiveDisable(Long id) {

        UserEntity user = UserEntity.builder()
                .isActive(false).build();

        final UserEntity userEntitySaved = userRepository.save(user);

        return UserDto.builder()
                .isActive(userEntitySaved.getIsActive()).build();
    }

    @Override
    public UserDto getUserByUsername(String username) {

        List<UserEntity> users = userRepository.findAll();

        for (UserEntity user : users) {
            if (user.getUserName().equals(username)) {
                return UserDto.builder()
                        .id(user.getId())
                        .userName(user.getUserName())
                        .firstName(user.getFirstName())
                        .lastName(user.getLastName())
                        .age(user.getAge())
                        .balance(user.getBalance())
                        .isActive(user.getIsActive()).build();

            }
        }
        throw new NotFoundException();
    }

    @Override
    public UserDto getUserBalanceByUsername(String username) {

        List<UserEntity> users = userRepository.findAll();

        for (UserEntity user : users) {
            if (user.getUserName().equals(username)) {
                return UserDto.builder()
                        .balance(user.getBalance()).build();
            }
        }

       throw new NotFoundException();
    }

    @Override
    public List<UserDto> getActiveUserList() {
        List<UserEntity> users = userRepository.findAll();
     //   List<UserEntity> users = userRepository.findByActive(true);
        List<UserDto> activeUsers = new ArrayList<>();

        for (UserEntity user : users) {
            if (user.getIsActive()) {
                UserDto userDto = UserDto.builder()
                        .id(user.getId())
                        .userName(user.getUserName())
                        .firstName(user.getFirstName())
                        .lastName(user.getLastName())
                        .age(user.getAge())
                        .balance(user.getBalance())
                        .isActive(user.getIsActive()).build();
                activeUsers.add(userDto);
            }
        }

        return activeUsers;
    }

    @Override
    public UserDto setIsActiveEnable(Long id) {
        UserEntity user = UserEntity.builder()
                .isActive(true).build();

        final UserEntity userEntitySaved = userRepository.save(user);

        return UserDto.builder()
                .isActive(userEntitySaved.getIsActive()).build();
    }


}









