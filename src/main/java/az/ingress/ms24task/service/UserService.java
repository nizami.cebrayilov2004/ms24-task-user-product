package az.ingress.ms24task.service;

import az.ingress.ms24task.dto.UserDto;
import az.ingress.ms24task.dto.UserRequestDto;

import java.util.List;

public interface UserService {
    UserDto create(UserRequestDto userRequestDto);

    UserDto getUserById(Long id);

    UserDto update(Long id, UserRequestDto userRequestDto);

    UserDto setIsActiveEnable(Long id);

    UserDto setIsActiveDisable(Long id);

    UserDto getUserByUsername(String username);

    UserDto getUserBalanceByUsername(String username);

    List<UserDto> getActiveUserList();


}
