package az.ingress.ms24task.rest;

import az.ingress.ms24task.dto.ProductDto;
import az.ingress.ms24task.dto.ProductRequestDto;
import az.ingress.ms24task.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
//import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public ProductDto create(@RequestBody @Validated ProductRequestDto productRequestDto){
        return productService.create(productRequestDto);
    }


    @GetMapping("/{id}")
    public ProductDto getProductById(@PathVariable Long id) {
        return productService.getProductById(id);
    }

    @PutMapping("{id}")
    public ProductDto updatePriceAndName(@PathVariable Long id, @RequestBody ProductRequestDto productRequestDto) {
        return productService.updatePriceAndName(id, productRequestDto);
    }

    @DeleteMapping("/{id1}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable Long id1) {
        productService.delete(id1);
    }
    @GetMapping("/list")
    public List<ProductDto> getAllUserList(){
        return productService.getAllUserList();
    }

    @PostMapping("/{userId}/buy")
    public List<ProductDto> buyProductByUser(@PathVariable Long userId, List<Long> productId){
        return productService.buyProductByUser(userId, productId);
    }

   }

