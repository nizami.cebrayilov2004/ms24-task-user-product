package az.ingress.ms24task.rest;

import az.ingress.ms24task.dto.UserDto;
import az.ingress.ms24task.dto.UserRequestDto;
import az.ingress.ms24task.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping
public class UserController {
    private final UserService userService;

    @PostMapping
    public UserDto create(@RequestBody @Validated UserRequestDto userRequestDto) {
        return userService.create(userRequestDto);
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @GetMapping("/username/{username}")
    public UserDto getUserByUsername(@PathVariable String username) {
        return userService.getUserByUsername(username);
    }

    @GetMapping("/balance/{username}")
    public UserDto getUserBalanceByUsername(@PathVariable String username) {
        return userService.getUserBalanceByUsername(username);
    }

    @PutMapping("/{id}")
    public UserDto update(@PathVariable Long id, @RequestBody UserRequestDto userRequestDto) {
        return userService.update(id, userRequestDto);
    }

    @PutMapping("/{id}/enable")
    public UserDto isActiveEnable(@PathVariable Long id) {
        return userService.setIsActiveEnable(id);
    }

    @PutMapping("/{id}/disable")
    public UserDto isActiveDisable(@PathVariable Long id) {
        return userService.setIsActiveDisable(id);
    }

    @GetMapping("/active")
    public List<UserDto> getActiveUserList() {
        return userService.getActiveUserList();
    }


}










