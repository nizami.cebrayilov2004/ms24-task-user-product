package az.ingress.ms24task.repository;

import az.ingress.ms24task.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
