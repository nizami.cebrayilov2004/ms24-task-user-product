package az.ingress.ms24task.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDto {

    private Long id;
    private String firstName;
    private Long price;
    private Long stockCount;
}
