package az.ingress.ms24task.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRequestDto {

    @NotBlank
    private String firstName;
    @NotBlank
    private String userName;
    @NotBlank
    private String lastName;
    private Boolean isActive;
    @NotNull
    @Positive
    private Integer age;
    private Long balance;
}
