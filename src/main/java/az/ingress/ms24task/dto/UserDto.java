package az.ingress.ms24task.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDto {

    private Long id;
    private String userName;
    private String firstName;
    private String lastName;
    private Long balance;
    private Boolean isActive;
    private Integer age;

}
